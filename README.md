# ohos_videocompressor

## Introduction
videoCompressor is a high-performance video compressor for OpenHarmony.

Currently, videoCompressor provides APIs to enable video compression.
## How to Download
You can download the library in either of the following ways:
1. Run the following command:
```
git clone https://gitee.com/openharmony-sig/ohos_videocompressor.git --recurse-submodules
```
2. Click **Download** to download the library to the local host, and save the [third_party_bounds_checking_function](https://gitee.com/openharmony/third_party_bounds_checking_function) code to the **videoCompressor/src/cpp/boundscheck** directory.


## How to Install

```
ohpm install @ohos/videocompressor
```

OpenHarmony ohpm
For details about the environment configuration, see [Installing the OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## Configuring the x86 Emulator

Running Your App/Service on an Emulator.

## How to Use

### Example of Using the Video Compression API.

```
let videoCompressor = new VideoCompressor();
videoCompressor.compressVideo(getContext(),this.selectFilePath,CompressQuality.COMPRESS_QUALITY_HIGH).then((data) => {
    if (data.code == CompressorResponseCode.SUCCESS) {
        console.log("videoCompressor HIGH message:" + data.message + "--outputPath:" + data.outputPath);
    } else {
        console.log("videoCompressor HIGH code:" + data.code + "--error message:" + data.message);
    }
    }).catch((err) => {
        console.log("videoCompressor HIGH get error message" + err.message);
    })
```

## Supported Video Specifications
Supported demuxing formats:

| Media| Demuxing Format    |
| -------- | ------------ |
| Video    | mp4 and mpeg.ts|

Supported video decoding formats:

| Video Decoding Type                 |
|-------------------------|
| AVC (H.264) and HEVC (H.265)|

Supported audio decoding formats:

| Audio Decoding Type  |
| -------------- |
| AAC |

Supported video encoding formats:

| Video Decoding Type           |
| ----------------------- |
| AVC (H.264) and HEVC (H.265)|

Supported audio encoding formats:

| Audio Encoding Type|
| ------------ |
| AAC          |

Supported muxing formats:

| Media| Muxing Format    |
| -------- | ------------ |
| Video    | mp4|

## Available APIs

| API                                                      | Parameter                                                        | Description    |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------ |
| compressVideo(context: Context, inputFilePath: string, quality: CompressQuality): Promise<CompressorResponse> | **context** indicates the context; **inputFilePath** indicates the path of the video; and **quality** indicates the quality of the compressed video.| Compresses a video.|

For details about unit test cases, see [TEST.md](./TEST.md).
## Constraints

This project has been verified in the following version:

DevEco Studio: (5.0.3.500), SDK: API 12 (5.0.0.25)
DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)

## Directory Structure

```
|----ohos_videocompressor
|     |---- entry  # Sample code
|     |---- videoCompressor  # ohos_videocompressor library
|           |---- index.ets  # External APIs
|     |---- README.md  # Readme
```

## How to Contribute

If you find any problem during the use, submit an [issue](https://gitee.com/openharmony-sig/ohos_videocompressor/issues) or
a [PR](https://gitee.com/openharmony-sig/ohos_videocompressor/pulls).

## License

This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/ohos_videocompressor/blob/master/LICENSE).
