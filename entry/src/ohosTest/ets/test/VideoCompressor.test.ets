/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
import { describe, expect, it } from '@ohos/hypium';
import { CompressorResponseCode, CompressQuality, VideoCompressor } from '@ohos/videoCompressor';
import fs from '@ohos.file.fs';
import AbilityDelegatorRegistry from '@ohos.app.ability.abilityDelegatorRegistry';

export default function abilityTest() {
  describe("videoCompressorTest", () => {

    let writeFile:(path: string, content: ArrayBuffer | string) => void = (path: string, content: ArrayBuffer | string): void => {
      try {
        console.log("videoCompressor write");
        let file = fs.openSync(path, fs.OpenMode.READ_WRITE | fs.OpenMode.CREATE);
        fs.writeSync(file.fd, content);
        fs.fsyncSync(file.fd);
        fs.closeSync(file.fd);
      } catch (e) {
        console.log("videoCompressor write error:" + JSON.stringify(e));
      }
    }

    it('videoCompressorHighTest', 0 ,async (done: Function) => {
      console.log("videoCompressor videoCompressorHighTest");
      let context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
      let path = context.cacheDir + "/";
      console.log("videoCompressor VideoCompressorHighTest:" + path);
      let buffer = await context.resourceManager.getRawFileContent('www.mp4');
      console.log("videoCompressor VideoCompressorHighTest buffer size:" + buffer.byteLength);
      writeFile(path + "www.mp4", buffer.buffer);

      let videoCompressor = new VideoCompressor();
      videoCompressor.compressVideo(context, path + "www.mp4", CompressQuality.COMPRESS_QUALITY_HIGH)
        .then((data) => {
          if (data.code == CompressorResponseCode.SUCCESS) {
            console.log("videoCompressor VideoCompressorHighTest message:" + data.message + "--outputPath:" + data.outputPath);
            expect(data.code).assertEqual(CompressorResponseCode.SUCCESS);
            done()
          } else {
            console.log("videoCompressor VideoCompressorHighTest code:" + data.code + "--error message:" + data.message);
            expect(data.code).not().assertEqual(CompressorResponseCode.SUCCESS);
            done()
          }
        }).catch((err: Error) => {
        console.log("videoCompressor VideoCompressorHighTest get error message" + err.message);
        expect(1).assertEqual(2)
        done()
      })
    })
    it('videoCompressorMediumTest', 0 ,async (done: Function) => {
      console.log("videoCompressor videoCompressorMediumTest");
      let context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
      let path = context.cacheDir + "/";
      console.log("videoCompressor videoCompressorMediumTest:" + path);
      let buffer = await context.resourceManager.getRawFileContent('www.mp4');
      console.log("videoCompressor videoCompressorMediumTest buffer size:" + buffer.byteLength);
      writeFile(path + "www.mp4", buffer.buffer);

      let videoCompressor = new VideoCompressor();
      videoCompressor.compressVideo(context, path + "www.mp4", CompressQuality.COMPRESS_QUALITY_MEDIUM)
        .then((data) => {
          if (data.code == CompressorResponseCode.SUCCESS) {
            console.log("videoCompressor videoCompressorMediumTest message:" + data.message + "--outputPath:" + data.outputPath);
            expect(data.code).assertEqual(CompressorResponseCode.SUCCESS);
            done()
          } else {
            console.log("videoCompressor videoCompressorMediumTest code:" + data.code + "--error message:" + data.message);
            expect(data.code).not().assertEqual(CompressorResponseCode.SUCCESS);
            done()
          }
        }).catch((err: Error) => {
        console.log("videoCompressor videoCompressorMediumTest get error message" + err.message);
        expect(1).assertEqual(2)
        done()
      })
    })
    it('videoCompressorLowTest', 0 ,async (done: Function) => {
      console.log("videoCompressor videoCompressorLowTest");
      let context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
      let path = context.cacheDir + "/";
      console.log("videoCompressor videoCompressorLowTest:" + path);
      let buffer = await context.resourceManager.getRawFileContent('www.mp4');
      console.log("videoCompressor videoCompressorLowTest buffer size:" + buffer.byteLength);
      writeFile(path + "www.mp4", buffer.buffer);

      let videoCompressor = new VideoCompressor();
      videoCompressor.compressVideo(context, path + "www.mp4", CompressQuality.COMPRESS_QUALITY_LOW)
        .then((data) => {
          if (data.code == CompressorResponseCode.SUCCESS) {
            console.log("videoCompressor videoCompressorLowTest message:" + data.message + "--outputPath:" + data.outputPath);
            expect(data.code).assertEqual(CompressorResponseCode.SUCCESS);
            done()
          } else {
            console.log("videoCompressor videoCompressorLowTest code:" + data.code + "--error message:" + data.message);
            expect(data.code).not().assertEqual(CompressorResponseCode.SUCCESS);
            done()
          }
        }).catch((err: Error) => {
        console.log("videoCompressor videoCompressorLowTest get error message" + err.message);
        expect(1).assertEqual(2)
        done()
      })
    })
  })
}