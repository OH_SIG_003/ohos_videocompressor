/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
import { CompressorResponseCode, CompressQuality, VideoCompressor } from '@ohos/videoCompressor';
import picker from '@ohos.file.photoAccessHelper';

@Entry
@Component
struct Index {
  private selectVideoText: string | Resource = $r('app.string.choose_video');
  private highTextTest: string | Resource = $r('app.string.high_quality')
  private mediaTextTest: string | Resource = $r('app.string.medium_quality')
  private lowTextTest: string | Resource = $r('app.string.low_quality')
  startColor: Color = Color.Blue
  @State selectFilePath: string = ""

  selectVideo() {
    let that = this;
    try {
      let photoSelectOptions = new picker.PhotoSelectOptions();
      photoSelectOptions.MIMEType = picker.PhotoViewMIMETypes.VIDEO_TYPE;
      photoSelectOptions.maxSelectNumber = 1;
      let photoPicker = new picker.PhotoViewPicker();
      photoPicker.select(photoSelectOptions).then((PhotoSelectResult) => {
        that.selectFilePath = PhotoSelectResult.photoUris[0];
        console.info('videoCompressor select selectFilePath:' + that.selectFilePath)
      }).catch((err: object) => {
        console.error("videoCompressor select failed with err:" + err);
      })
    } catch (err) {
      console.error("videoCompressor select failed with err2:" + err);
    }
  }

  build() {
    Row() {
      Column() {
        Text(this.selectFilePath).fontSize(18).margin({ top: 30 }).fontWeight(FontWeight.Bold);
        Button(this.selectVideoText)
          .fontSize(20)
          .margin({ top: 30 })
          .height(50)
          .backgroundColor(this.startColor)
          .width("80%")
          .onClick(() => {
            console.log("selectVideo");
            this.selectVideo();
          })
        TextInput({ placeholder: $r('app.string.input_file_name') })
          .fontSize(20)
          .margin({ top: 30 })
          .width('80%')
          .onChange((value) => {
            this.selectFilePath = getContext().filesDir + "/" + value
          })
        Button(this.highTextTest)
          .fontSize(20)
          .margin({ top: 30 })
          .height(50)
          .backgroundColor(this.startColor)
          .width("80%")
          .onClick(() => {
            let videoCompressor = new VideoCompressor();
            videoCompressor.compressVideo(getContext(), this.selectFilePath, CompressQuality.COMPRESS_QUALITY_HIGH)
              .then((data) => {
                if (data.code == CompressorResponseCode.SUCCESS) {
                  console.log("videoCompressor HIGH message:" + data.message + "--outputPath:" + data.outputPath);
                } else {
                  console.log("videoCompressor HIGH code:" + data.code + "--error message:" + data.message);
                }
              }).catch((err: Error) => {
              console.log("videoCompressor HIGH get error message" + err.message);
            })
          })

        Button(this.mediaTextTest)
          .fontSize(20)
          .margin({ top: 30 })
          .height(50)
          .backgroundColor(this.startColor)
          .width("80%")
          .onClick(() => {
            let videoCompressor = new VideoCompressor();
            videoCompressor.compressVideo(getContext(), this.selectFilePath, CompressQuality.COMPRESS_QUALITY_MEDIUM)
              .then((data) => {
                if (data.code == CompressorResponseCode.SUCCESS) {
                  console.log("videoCompressor MEDIUM message:" + data.message + "--outputPath:" + data.outputPath);
                } else {
                  console.log("videoCompressor MEDIUM code:" + data.code + "--error message:" + data.message);
                }
              }).catch((err: Error) => {
              console.log("videoCompressor HIGH get error message" + err.message);
            })
          })
        Button(this.lowTextTest)
          .fontSize(20)
          .margin({ top: 30 })
          .height(50)
          .backgroundColor(this.startColor)
          .width("80%")
          .onClick(() => {
            let videoCompressor = new VideoCompressor();
            videoCompressor.compressVideo(getContext(), this.selectFilePath, CompressQuality.COMPRESS_QUALITY_LOW)
              .then((data) => {
                if (data.code == CompressorResponseCode.SUCCESS) {
                  console.log("videoCompressor LOW message:" + data.message + "--outputPath:" + data.outputPath);
                } else {
                  console.log("videoCompressor LOW code:" + data.code + "--error message:" + data.message);
                }
              }).catch((err: Error) => {
              console.log("videoCompressor LOW get error message" + err.message);
            })
          })
      }
      .width('100%')
    }
    .height('100%')
  }
}